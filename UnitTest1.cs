using OpenQA.Selenium;
using SendEmailTaskLocators.PageObject;

namespace SendEmailTaskLocators
{
    public class Tests
    {
        public IWebDriver _driver;

        private const string login = "epam2605";
        private const string password = "epam11224455";
        private const string sendto = "lyudmilachaban@gmail.com";
        private const string subject = "test";
        private const string message = "test text";

        [SetUp]
        public void Setup()
        {

            _driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            _driver.Navigate().GoToUrl("https://accounts.ukr.net/login?client_id=9GLooZH9KjbBlWnuLkVX");
            _driver.Manage().Window.Maximize();

        }

        [Test]
        public void Test1()
        {
            var mainpage = new MainPageObject(_driver);
            mainpage
                .SignIn(login, password)
                .CreateNewMail(sendto, subject, message);

            
            
        }
    }
}