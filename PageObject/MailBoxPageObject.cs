﻿using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SendEmailTaskLocators.PageObject
{
    internal class MailBoxPageObject
    {
        private readonly IWebDriver _driver;

        private readonly By _createNewMailButton = By.XPath("//button[text()=\"Написати листа\"]");
        private readonly By _mailToField = By.Name("toFieldInput");
        private readonly By _subjectField = By.XPath("//input[contains(@name, \"subject\")]");
        private readonly By _textField = By.ClassName("mce-plaintext-area");
               
        
        private readonly By _sendButton = By.XPath("//button[contains(@class,\"button primary send\")]");
         private readonly By _success = By.CssSelector("div[class=\"sendmsg__ads-ready\"]\r\n");


        public MailBoxPageObject(IWebDriver driver)
        {
            _driver = driver;
        }

        

        public void CreateNewMail(string sendto, string subject, string message)
        {
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);

            _driver.FindElement(_createNewMailButton).Click();

            _driver.FindElement(_mailToField).SendKeys(sendto);
            _driver.FindElement(_subjectField).SendKeys(subject);
            _driver.FindElement(_textField).Click();
            _driver.FindElement(_textField).SendKeys(message);
            _driver.FindElement(_sendButton).Click();
            


            Assert.That(_driver.FindElements(_success).Any(), Is.True);

        }
    }
}
