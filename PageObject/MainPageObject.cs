﻿using OpenQA.Selenium;
using OpenQA.Selenium.DevTools;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendEmailTaskLocators.PageObject
{
    internal class MainPageObject
    {
        private readonly IWebDriver _driver;

        private readonly By _loginField = By.Name("login");
        private readonly By _passwordField = By.XPath("//input[contains(@name, \"password\")]");
        private readonly By _signinButton = By.XPath("//button[contains(@type,\"submit\")]");


        private readonly By _inboxButton = By.CssSelector("button[class=\"button primary compose\"]");


        public MainPageObject(IWebDriver driver)
        {
            _driver = driver;
        }

        public MailBoxPageObject SignIn(string login, string password)
        {
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            
            _driver.FindElement(_loginField).SendKeys(login);
            _driver.FindElement(_passwordField).SendKeys(password);
            _driver.FindElement(_signinButton).Click();
            _driver.FindElement(_inboxButton).Click();

            return new MailBoxPageObject(_driver);

        }
    }
}
